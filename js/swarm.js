/*
The MIT License (MIT)

Copyright (c) <year> <copyright holders>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


// fix modulus
Number.prototype.mod = function(n) {
    return ((this%n)+n)%n;
}

// fetch GET variables
function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}



/**
* This class describes the boids of the swarm simulation. Each boid is an
* autonomous entity that is fully controlled by its individual parameters.
**/
function Boid(xlim, ylim, minSpeed, maxSpeed) {
    /**
    * Move the boid according to its current velocity. If the current velocity
    * is higher that the defined maximum speed, scale the values so the largest
    * of them matches the allowed maximum.
    **/
    this.step = function() {
        //divisor = (this.velocity[0] > this.velocity[1]) ? this.velocity[0] : this.velocity[1];
        var sign = {};
        sign[0] = this.velocity[0] >= 0 ? 1 : -1;
        sign[1] = this.velocity[1] >= 0 ? 1 : -1;
        divisor = Math.sqrt((this.velocity[0] * this.velocity[0]) + (this.velocity[1] * this.velocity[1]));

        if (divisor == 0) {
            this.velocity[0] = sign[0] * this.minSpeed;
            this.velocity[1] = sign[1] * this.minSpeed;
        } else {
            this.velocity[0] = sign[0] * this.minSpeed + (this.maxSpeed - this.minSpeed) * (this.velocity[0] / divisor);
            this.velocity[1] = sign[1] * this.minSpeed + (this.maxSpeed - this.minSpeed) * (this.velocity[1] / divisor);
        }
        
        this.position[0] = (this.position[0] + this.velocity[0]).mod(this.xlim);
        this.position[1] = (this.position[1] + this.velocity[1]).mod(this.ylim);
    };
    
    
    /**
    * Calculate the vector of the boid to a point in space.  Note that the
    * canvas wraps on all sides. This means if the actual distance is higher
    * that half the canvas size, the function needs to wrap and calculate the
    * vector in the other direction.
    **/
    this.vector = function(point) {
        var diff = new Array(0, 0);
        diff[0] = point[0] - this.position[0];
        diff[1] = point[1] - this.position[1];

        if (Math.abs(diff[0]) > this.xlim/2) {
            var n_sign = diff[0] ? diff[0] < 0 ? 1 : -1 : 0;
            diff[0] = n_sign * (this.xlim - Math.abs(diff[0]));
        }
        
        if (Math.abs(diff[1]) > this.ylim/2) {
            var n_sign = diff[1] ? diff[1] < 0 ? 1 : -1 : 0;
            diff[1] = n_sign * (this.ylim - Math.abs(diff[1]));
        }

        return diff;
    };

    /**
    *   Calculate the distance of a boids to a point in space using the vector
    *   function.
    **/
    this.distance = function(point) {
        var vector = this.vector(point);
        return Math.sqrt((vector[0] * vector[0]) + (vector[1] * vector[1]));
    };
    
    
    /**
    * Draw the boid at its current location.
    **/
    this.draw = function(context) {
        context.fillStyle = "#000000";
        context.fillRect(this.position[0] - 1, this.position[1] - 1, 3, 3);
    };
    
    
    /**
    * Initialize the boid with random position and velocity in their respective
    * limits.
    **/
    this.xlim = xlim;
    this.ylim = ylim;

    this.maxSpeed = maxSpeed;
    this.minSpeed = minSpeed;
    
    this.position = new Array(0, 0);
    this.position[0] = Math.random() * this.xlim;
    this.position[1] = Math.random() * this.ylim;
    
    this.velocity = new Array(0, 0);
    var sign  = Math.random() < 0.5 ? 1 : -1;
    this.velocity[0] = sign * (this.minSpeed + Math.random() * (this.maxSpeed - this.minSpeed));
    sign  = Math.random() < 0.5 ? 1 : -1;
    this.velocity[1] = sign * (this.minSpeed + Math.random() * (this.maxSpeed - this.minSpeed));
}



function Swarm(num, canvas, parameters) {
    /**
    * Clear the canvas and update the position of all boids according to their
    * velocity. Finally draw all boids at their new positions. The velocity of
    * the boids depends on the thre forces separation, alignment and cohesion
    * that are calculated according to their neighbors.
    **/
    this.step = function() {
        this.context.clearRect(0, 0, this.canvasWidth, this.canvasHeight);
        this.context.fillStyle = "#eeeeee";
        this.context.fillRect(0, 0, this.canvasWidth, this.canvasHeight);
        
        for (var i = 0; i < this.num; i++) {
            var separation = new Array(0, 0);
            var separationCount = 0;
            var alignment = new Array(0, 0);
            var cohesion = new Array(0, 0);

            var neighbors = this.getNeighbors(i);
            var numNeighbors = neighbors.length;
            for (var j = 0; j < numNeighbors; j++) {
                position = neighbors[j].position;
                velocity = neighbors[j].velocity;
                vector = this.boids[i].vector(position);
                
                if (this.boids[i].distance(position) < 10) {
                    separation[0] -= vector[0];
                    separation[1] -= vector[1];
                    separationCount++;
                } else {
                    cohesion[0] += vector[0];
                    cohesion[1] += vector[1];
                }

                alignment[0] += velocity[0];
                alignment[1] += velocity[1];
                
            }

            if (separationCount > 0) {
                separation[0] /= separationCount;
                separation[1] /= separationCount;
            }

            if (numNeighbors > 0) {
                alignment[0] /= numNeighbors;
                alignment[1] /= numNeighbors;
            }

            if (numNeighbors - separationCount > 0) {
                cohesion[0] /= numNeighbors-separationCount;
                cohesion[1] /= numNeighbors-separationCount;
            }

            //this.boids[i].velocity[0] = cohesion[0]*this.cohesion + separation[0]*this.separation + alignment[0]*this.alignment;
            //this.boids[i].velocity[1] = cohesion[1]*this.cohesion + separation[1]*this.separation + alignment[1]*this.alignment;
            
            this.boids[i].velocity[0] = (this.inertia * this.boids[i].velocity[0] + (1-this.inertia) * (cohesion[0]*this.cohesion + separation[0]*this.separation + alignment[0]*this.alignment)) / 2;
            this.boids[i].velocity[1] = (this.inertia * this.boids[i].velocity[1] + (1-this.inertia) * (cohesion[1]*this.cohesion + separation[1]*this.separation + alignment[1]*this.alignment)) / 2;
            
            this.boids[i].step();
            this.boids[i].draw(this.context);
        }
    };
    
    
    /**
    * Get all neighbors of a specific boid. Neighbors are boids that are within
    * the radius specified by the initialization routine of the swarm.
    **/
    this.getNeighbors = function(index) {
        var neighbors = new Array();

        for (var i = 0; i < this.num; i++) {
            if (this.boids[index].distance(this.boids[i].position) <= this.radius) {
                v1 = this.boids[index].velocity;
                v2 = this.boids[index].vector(this.boids[i].position);

                // compute the cosine of the angle
                theta = (v1[0]*v2[0] + v1[1]*v2[1]) / (Math.sqrt(v1[0]*v1[0]+v1[1]*v1[1]) * Math.sqrt(v2[0]*v2[0]+v2[1]*v2[1]))
                
                if (theta > -0.7)
                    neighbors.push(this.boids[i]);
            }
        }

        return neighbors;
    };


    /**
    * Initialize the swarm and the canvas as well as their parameters, then run
    * the simulation.
    **/
    this.num = num;
    
    canvas = document.getElementById(canvas);
    this.context = canvas.getContext("2d");
    
    this.canvasWidth = this.context.canvas.width;
    this.canvasHeight = this.context.canvas.height;

    this.minSpeed = 1;
    this.maxSpeed = 3;
    this.radius = 50;

    this.separation = parameters['separation'];
    this.alignment = parameters['alignment'];
    this.cohesion = parameters['cohesion'];
    this.inertia = parameters['inertia'];

    if (this.separation == undefined)
        this.separation = 10;
    if (this.alignment == undefined)
        this.alignment = 20;
    if (this.cohesion == undefined)
        this.cohesion = 1;
    if (this.inertia == undefined)
        this.inertia = 0.8;


    this.boids = new Array(this.num);

    // Initialize the boids.
    for (var i = 0; i < this.num; i++) {
        this.boids[i] = new Boid(this.canvasWidth, this.canvasHeight, this.minSpeed, this.maxSpeed);
    }
}
